import sublime
import sublime_plugin
import base64

settings_file = "ImsiEncoder.sublime-settings"


class ImsiEncoderCommand(sublime_plugin.TextCommand):
    def run(self, edit, eapPayload=True):
        if int(sublime.version()) >= 3000:
            sels = self.view.sel()
            for region in sels:
                if not region.empty():
                    imsi = self.view.substr(region)
                    # nai = '0' + imsi.strip() + '@nai.epc.mnc120.mcc310.3gppnetwork.org'
                    mnc = self.getMnc()
                    mcc = self.getMcc()
                    if mcc == "":
                        sublime.message_dialog('The MCC is not configured.\nPlease set the proper value and try again.')
                        self.view.window().run_command('set_mcc')
                        break
                    if mnc == "":
                        sublime.message_dialog('The MNC is not configured.\nPlease set the proper value and try again.')
                        self.view.window().run_command('set_mnc')
                        break
                    nai = '0' + imsi.strip() + '@nai.epc.mnc' + mnc + '.mcc' + mcc + '.3gppnetwork.org'
                    bhex = base64.b16encode(nai.encode())
                    if eapPayload is True:
                        bhex = b'0200003b01' + bhex
                    shex = bhex.decode()
                    base_64 = base64.encodebytes(bytes.fromhex(shex))
                    subId = base_64.decode('ascii').replace('\n', '')
                    self.view.replace(edit, region, subId)
        else:
            sublime.error_message('The plugin ImsiEncoder is only supported in Sublime Text 3.\nIt will be available for older versions in a future release.')

    def getMnc(self):
        s = sublime.load_settings(settings_file)
        return str(s.get("mnc", ""))

    def getMcc(self):
        s = sublime.load_settings(settings_file)
        return str(s.get("mcc", ""))


class SetMncCommand(sublime_plugin.WindowCommand):

    def run(self):
        s = sublime.load_settings(settings_file)
        self.window.show_input_panel("Set default MNC value:", str(s.get("mnc", "")), self.on_done, None, None)

    def on_done(self, text):
        if text:
            s = sublime.load_settings(settings_file)
            s.set("mnc", text)
            sublime.save_settings(settings_file)
        else:
            sublime.error_message('The entered value is not valid.')
            self.window.run_command('set_mnc')


class SetMccCommand(sublime_plugin.WindowCommand):

    def run(self):
        s = sublime.load_settings(settings_file)
        self.window.show_input_panel("Set default MCC value:", str(s.get("mcc", "")), self.on_done, None, None)

    def on_done(self, text):
        if text:
            s = sublime.load_settings(settings_file)
            s.set("mcc", text)
            sublime.save_settings(settings_file)
        else:
            sublime.error_message('The entered value is not valid.')
            self.window.run_command('set_mcc')
